import numpy as np
from matplotlib import pyplot as plt
import random

class Experiment(object):

    def __init__(self, id, x, y, labels, normed_y=None, samples=None, model=None, dir=None, is_test=False):
        self.id = id
        self.x = x
        self.y = y
        self.labels = labels
        self.max_val = np.max(y)
        self.min_val = np.min(y)
        self.normed_y = normed_y if normed_y is not None else self.normalizeY()
        self.is_test = is_test
        self.mod_name = model

        if model is not None:
            if model == 'deepant':
                self.trainX, self.trainy, self.train_labels, self.testX, self.testy, self.test_labels, self.train_label_layer, self.test_label_layer = self.makeDeepANTSamples(dir)
            elif model == 'unet':
                self.samples, self.s_labels, self.anomalies, self.non_anomalies, self.anomalous_labels, self.nonanomalous_labels, self.label_layer, self.anom_label_layer, self.nonanom_label_layer = self.makeUNETSamples()
            elif model == 'lstm':
                self.samples, self.s_labels, self.anomalies, self.non_anomalies, self.anomalous_labels, self.anom_next_val, self.nonanom_next_val, self.nonanomalous_labels, self.testy, self.label_layer, self.anom_label_layer, self.nonanom_label_layer = self.makeLSTMSamples()
            elif model == 'mine':
                self.samples, self.s_labels, self.anomalies, self.non_anomalies, self.anomalous_labels, self.nonanomalous_labels, self.label_layer, self.anom_label_layer, self.nonanom_label_layer = self.makeUNETSamples(model='mine')
        else:
            self.samples, self.s_labels, self.anomalies, self.non_anomalies = samples if samples is not None else self.makeSamples()

        if is_test:
            self.threshold = self.getThreshold(model)

    def augmentMe(self, per_sample):
        new_y = self.y
        new_labels = self.labels
        start = 0
        end = 32
        step = 32
        idxs = random.sample(range(0, 32), per_sample)

        while end < self.y.shape[0]:
            current = self.y[start:end]
            if np.count_nonzero(current) == 0:
                new_y[start + idxs[0]] = new_y[start + idxs[0]] * 2
                new_y[start + idxs[1]] = new_y[start + idxs[1]] * 3
                new_y[start + idxs[2]] = new_y[start + idxs[2]] / 3

                new_labels[start + idxs[0]] = 1
                new_labels[start + idxs[1]] = 1
                new_labels[start + idxs[2]] = 1

            start+=step
            end+=step
        return new_y, new_labels

    def getThreshold(self, model):
        if model == 'deepant':
            for i, val in enumerate(self.train_label_layer):
                if np.count_nonzero(val) == 0:
                    nonanom_sample = self.trainX[i]
        else:
            nonanom_sample = self.samples[0]
        thresh = np.amax(np.absolute(np.diff(nonanom_sample.flatten())))
        return thresh * 1.5

    def graphExperiment(self, y):
        x = np.linspace(0, y.shape[0], y.shape[0])
        plt.figure(figsize=(14,5))
        plt.plot(x, y)
        plt.show()

    def makeDeepANTSamples(self, dir):
        start = 0
        if dir == 'A1Benchmark' or dir == 'A2Benchmark':
            end = 45
        else:
            end = 35
        step = 1
        trainX = []
        train_labels = []
        trainy = []
        train_label_layer = []

        testX = []
        testy = []
        test_labels = []
        test_label_layer = []
        label_layer, anom_label_layer, nonanom_label_layer = [], [], []


        while end < (self.x.shape[0] * .4):
            sample = self.normed_y[start:end]
            if self.normed_y[end] != 1 or np.count_nonzero(np.array(self.labels[start:end])) == 0:
                trainX.append(np.expand_dims(sample, axis=1))
                trainy.append(self.normed_y[end])
                train_labels.append(self.labels[end])
                train_label_layer.append(np.array(self.labels[start:end]))
            start+=step
            end+=step

        while end < (self.x.shape[0]-1):
            sample = self.normed_y[start:end]
            testX.append(np.expand_dims(sample, axis=1))
            testy.append(self.normed_y[end])
            test_labels.append([self.labels[end]])
            test_label_layer.append(np.array(self.labels[start:end]))
            start+=step
            end+=step
        return np.array(trainX), np.array(trainy), np.array(train_labels), np.array(testX), np.array(testy), np.array(test_labels), np.array(train_label_layer), np.array(test_label_layer)

    def makeLSTMSamples(self, model=None):
        start = 0
        if model == 'deepant':
            end = 45
        else:
            end = 32
        step = 1
        trainX = []
        train_labels = []
        trainy = []
        samples = []
        anomalies = []
        non_anomalies = []
        s_labels = []
        nonanomalous_labels = []
        anomalous_labels = []
        label_layer, anom_label_layer, nonanom_label_layer = [], [], []
        nonanom_next_val, anom_next_val = [], []

        testX = []
        testy = []
        test_labels = []

        while end < self.x.shape[0]:
            sample = self.normed_y[start:end]
            samples.append(np.expand_dims(sample, axis=1))
            testy.append(self.labels[end])
            toadd = np.expand_dims(sample, axis=1)
            label_layer.append(np.array(self.labels[start:end]))
            if np.count_nonzero(self.labels[start:end+1]) > 0:
                anomalies.append(toadd)
                anom_next_val.append(np.array(self.normed_y[end]))
                s_labels.append(np.array(self.normed_y[end]))
                anomalous_labels.append(self.labels[end])
                anom_label_layer.append(np.array(self.labels[start:end]))
            else:
                non_anomalies.append(toadd)
                nonanom_next_val.append(np.array(self.normed_y[end]))
                s_labels.append(np.array(self.normed_y[end]))
                nonanomalous_labels.append(self.labels[end])
                nonanom_label_layer.append(np.array(self.labels[start:end]))

            start+=step
            end+=step
        return np.array(samples), np.array(s_labels), np.array(anomalies), np.array(non_anomalies), np.array(anomalous_labels), np.array(anom_next_val), np.array(nonanom_next_val), np.array(nonanomalous_labels), np.array(testy), np.array(label_layer), np.array(anom_label_layer), np.array(nonanom_label_layer)



    def makeUNETSamples(self, model=None):
        if model is not None:
            start = 0
            end = 32
            step = 8
        else:
            start = 0
            end = 32
            step = 8
        samples = []
        anomalies = []
        non_anomalies = []
        s_labels = []
        anomalous_labels = []
        nonanomalous_labels = []
        label_layer, anom_label_layer, nonanom_label_layer = [], [], []

        while end < self.x.shape[0]:
            sample = self.normed_y[start:end]
            samples.append(np.expand_dims(sample, axis=1))
            toadd = np.expand_dims(sample, axis=1)
            label_layer.append(np.array(self.labels[start:end]))
            if np.count_nonzero(self.labels[start:end]) > 0:
                anomalies.append(toadd)
                s_labels.append(np.expand_dims(self.labels[start:end], axis=1))
                anomalous_labels.append(np.expand_dims(self.labels[start:end], axis=1))
                if model is not None:
                    anom_label_layer.append(np.concatenate((np.array(self.labels[start:(end - step)]), np.array([0.5,0.5,0.5,0.5])), axis=0))
                else:
                    anom_label_layer.append(np.concatenate((np.array(self.labels[start:(end - step)]), np.array([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5])), axis=0))
            else:
                non_anomalies.append(toadd)
                s_labels.append(np.expand_dims(self.labels[start:end], axis=1))
                if model is not None:
                    nonanom_label_layer.append(np.concatenate((np.array(self.labels[start:(end - step)]), np.array([0.5,0.5,0.5,0.5])), axis=0))
                else:
                    nonanom_label_layer.append(np.concatenate((np.array(self.labels[start:(end - step)]), np.array([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5])), axis=0))

            start+=step
            end+=step
        return np.array(samples), np.array(s_labels), np.array(anomalies), np.array(non_anomalies), np.array(anomalous_labels), np.array(nonanomalous_labels), np.array(label_layer), np.array(anom_label_layer), np.array(nonanom_label_layer)

    def makeSamples(self):
        start = 0
        end = 64
        step = 16
        samples = []
        anomalies = []
        non_anomalies = []
        s_labels = []

        while end < self.x.shape[0]:
            sample = self.normed_y[start:end]
            # graphSample(list(range(start, end)), sample)
            samples.append(np.expand_dims(sample, axis=1))
            if np.count_nonzero(self.labels[start:end]) > 0:
                toadd = np.expand_dims(sample, axis=1)
                anomalies.append(toadd)
                s_labels.append(np.ones_like(toadd))
            else:
                toadd = np.expand_dims(sample, axis=1)
                non_anomalies.append(toadd)
                s_labels.append(np.zeros_like(toadd))
            start+=step
            end+=step
        return np.array(samples), np.array(s_labels), np.array(anomalies), np.array(non_anomalies)

    def normalizeY(self):
        current = self.y
        maxs = np.full((current.shape[0], ), self.max_val)
        mins = np.full((current.shape[0], ), self.min_val)

        numerator = np.subtract(current, mins)
        denom = np.subtract(maxs, mins)
        return np.divide(numerator, denom)

    def __repr__(self):
        return "Experiment(id: {}, Length: {}, Anomaly_amt: {}, max: {}, min: {}, samples: {}".format(self.id, len(self.x), np.count_nonzero(self.labels), self.max_val, self.min_val, len(self.samples))

def graphSample(x, y):
    plt.figure(figsize=(14,5))
    plt.plot(x, y)
    plt.show()
