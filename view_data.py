from matplotlib import pyplot as plt
import numpy as np
import sys




def scatter2d(data):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(data['noanom']['x'], data['noanom']['y'], c='g', alpha=.5, s=2, marker='o')
    ax.scatter(data['anom']['x'], data['anom']['y'], c='r', s=2, marker='o')
    plt.show()
    exit()



def line_plot_distance_fft(x, y, label):
    start = 0
    stop = 255
    step = 50

    while stop < x.shape[0]:
        # print(label[start:stop])
        plt.figure(figsize=(14,5))
        fft = np.absolute(np.fft.fft(y[start:stop]))
        fft = np.divide(fft[1:], fft[0])
        time = np.linspace(0, len(fft), len(fft))
        if np.count_nonzero(label[start:stop]) > 0:
            color = 'r'
        else:
            color = 'b'
        plt.plot(time, fft, c=color)
        plt.ylim(bottom=0, top=.4)
        # plt.xlim(left=0)
        plt.show()

        plt.show()
        start+=step
        stop+=step

def line_plot_distance(x, y, label):
    start = 0
    stop = x.shape[0]-1 #12
    step = 4

    maxs = np.full((y.shape[0], ), np.amax(y))
    mins = np.full((y.shape[0], ), np.amin(y))

    numerator = np.subtract(y, mins)
    denom = np.subtract(maxs, mins)
    y =  np.divide(numerator, denom)


    while stop < x.shape[0]:
        # print(label[start:stop])
        plt.figure(figsize=(14,5))
        cut_y = y[start:stop]
        time = np.linspace(0, len(cut_y), len(cut_y))
        # for i, val in enumerate(label[start:stop]):
        #     if val > 0:
        #         print(i)

        p = np.poly1d(np.polyfit(time, cut_y, 5))
        for i, val in enumerate(label):
            if val > 0:
                plt.axvline(x=i, c='red', linestyle='--')

        # if np.count_nonzero(label[start:stop]) > 0:
        #     color = 'r'
        # else:
        color = 'b'
        plt.plot(time, cut_y, c=color) #, c=color
        plt.title("A4", fontsize=22)
        plt.xlabel("Timesteps", fontsize=22)
        plt.ylabel("Value", fontsize=22)
        plt.xticks(fontsize=22)
        plt.yticks(fontsize=22)
        # plt.plot(time, p(time))
        # plt.ylim(bottom=0)
        # plt.xlim(left=0)
        plt.show()

        start+=step
        stop+=step


data = {'anom':{'x':[], 'y':[]},
            'noanom':{'x':[], 'y':[]}}

for i in range(1,101):
    print(i)
    with open('data/ydata-labeled-time-series-anomalies-v1_0/A1Benchmark/real_'+ str(i) +'.csv') as f:
        lines = (line for line in f if not line.startswith('#'))
        arr = np.loadtxt(lines, delimiter=',', skiprows=1)

    x = arr[:,0]
    y = arr[:,1]
    label = arr[:,2]


    for i, val in enumerate(label):
        if val == 1:
            data['anom']['x'].append(x[i])
            data['anom']['y'].append(y[i])
        else:
            data['noanom']['x'].append(x[i])
            data['noanom']['y'].append(y[i])



    # scatter2d(data)
    line_plot_distance(x, y, label)
    # line_plot_distance_fft(x, y, label)
    if i == 100:
        exit()
