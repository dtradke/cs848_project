import sys
import numpy as np
import tensorflow as tf
import process_data
from os import listdir
from os.path import isfile, join
import models
import math
import time
import pandas
import scipy.io


class ResultSummary(object):

    def __init__(self, benchmark, mod_type, TP, TN, FP, FN):
        self.benchmark = benchmark
        self.mod_type = mod_type
        self.tp = TP
        self.tn = TN
        self.fp = FP
        self.fn = FN
        self.precision = TP / (TP + FP)
        self.recall = TP / (TP + FN)
        try:
            self.f1 = 2 * ((self.precision * self.recall) / (self.precision + self.recall))
        except:
            self.f1 = 0

    def __repr__(self):
        return "ResultSummary(Exp: {}, Total Precision: {}, Total Recall: {}, Total F1: {}, Total TP: {}, Total TN: {}, Total FN: {}, Total FP: {})".format(self.benchmark, round(self.precision,2), round(self.recall,2), round(self.f1,2), self.tp, self.tn, self.fn, self.fp)


class Result(object):

    def __init__(self, name, mod, TP, TN, FP, FN, min_anom=0, max_anom=0, min_nonanom=0, max_nonanom=0):
        self.name = name
        self.mod = mod
        self.tp = TP
        self.tn = TN
        self.fp = FP
        self.fn = FN
        self.anom_thresholds = [min_anom, max_anom]
        self.nonanom_thresholds = [min_nonanom, max_nonanom]
        try:
            self.precision = TP / (TP + FP)
        except:
            self.precision = 0
        try:
            self.recall = TP / (TP + FN)
        except:
            self.recall = 0
        try:
            self.f1 = 2 * ((self.precision * self.recall) / (self.precision + self.recall))
        except:
            self.f1 = 0

    def __repr__(self):
        return "Result(Exp: {}, Precision: {}, Recall: {}, F1: {}, TP: {}, FN: {}, FP: {})".format(self.name[:-4], round(self.precision,2), round(self.recall,2), round(self.f1,2), self.tp, self.fn, self.fp)

def printAndWrite(best_results, model):
    df = pandas.DataFrame(data={"best_results": best_results})
    time_string = time.strftime("%Y%m%d-%H%M%S")
    fname = "./output/" + str(model) + str(time_string) + ".csv"
    df.to_csv(fname, sep=',',index=False)
    print("Finished")

def findLargestGap(a, n):
    max1 = -sys.maxsize - 1
    for i in range(0, n, 1):
        for j in range(0, n, 1):
            if (abs(a[i] - a[j]) > max1):
                max1 = abs(a[i] - a[j])
    return max1

def findBestResults(results):
    best_results = []
    for r in results.keys():
        exp_arr = results[r]
        best = exp_arr[0]
        for exp in exp_arr:
            if exp.f1 > best.f1:
                best = exp
        best_results.append(best)
    return best_results

def getMyModel(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.my_model(full_stats, 'models/' + i)
    return models.my_model(full_stats)

def getMyModelFFT(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.my_model_fft('models/' + i)
    return models.my_model_fft(full_stats)

def getUnet(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.unet(full_stats, 'models/' + i)
    return models.unet(full_stats)

def getDeepANTFFT(full_stats, dir):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.deepant_fft(dir,'models/' + i)
    return models.deepant_fft(full_stats, dir)

def getDeepANT(full_stats, dir):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.deepant(full_stats, dir, 'models/' + i)
    return models.deepant(full_stats, dir)

def getLSTM(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.lstm(full_stats, 'models/' + i)
    return models.lstm()

def getLSTM_all_noFFT(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.lstm_all_noFFT(full_stats, 'models/' + i)
    return models.lstm_all_noFFT(full_stats)

def getFFTUNET(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            return models.unet_fft('models/' + i)
    return models.unet_fft(full_stats)

def getFFTLSTM(full_stats):
    global loaded_model
    for i in sys.argv:
        if i[-2:] == 'h5':
            loaded_model = True
            if full_stats:
                return models.lstm_all('models/' + i)
            else:
                return models.lstm_fft('models/' + i)
    if full_stats:
        return models.lstm_all()
    else:
        return models.lstm_fft()

def flat_y(y):
    new_labels = []
    for i in y:
        ones = np.count_nonzero(i)
        if ones == 0:
            new_labels.append(np.array([1,0]))
        else:
            new_labels.append(np.array([0,1]))
    return np.array(new_labels)

def getDataStats(X, testX, X_ll, testX_ll):
    training = []
    testing = []
    for count, i in enumerate(X):
        flat = i.flatten()
        time = np.linspace(0, flat.shape[0], flat.shape[0])
        p = np.poly1d(np.polyfit(time, flat, 5))
        from_fit = np.expand_dims(np.array(np.absolute(np.subtract(flat, p(time)))), axis=1)

        means = np.ones_like(flat)
        means.fill(np.mean(flat))
        mean_diff = np.subtract(flat, means)
        new = []
        for j in range(0,(flat.shape[0] - X_ll[count].shape[0])):
            new.append(0.5)
        concat = np.concatenate((X_ll[count], np.array(new)), axis=0)
        # stacked = np.array(list(zip(flat, mean_diff, from_fit.flatten(), concat)))
        stacked = np.array(list(zip(flat, from_fit.flatten(), concat)))
        training.append(stacked)
    for count, i in enumerate(testX):
        flat = i.flatten()
        time = np.linspace(0, flat.shape[0], flat.shape[0])
        p = np.poly1d(np.polyfit(time, flat, 5))
        from_fit = np.expand_dims(np.array(np.absolute(np.subtract(flat, p(time)))), axis=1)

        means = np.ones_like(flat)
        means.fill(np.mean(flat))
        mean_diff = np.subtract(flat, means)
        new = []
        for j in range(0,(flat.shape[0] - X_ll[count].shape[0])):
            new.append(0.5)
        concat = np.concatenate((X_ll[count], np.array(new)), axis=0)
        stacked = np.array(list(zip(flat, from_fit.flatten(), concat)))
        # stacked = np.array(list(zip(flat, mean_diff, from_fit.flatten(), concat)))
        testing.append(stacked)
    return np.array(training), np.array(testing)

def getDataStatsLSTM(X, testX, X_ll, testX_ll):
    training = []
    testing = []
    train_ll = []
    test_ll = []
    for count, i in enumerate(X):
        flat = i.flatten()
        means = np.ones_like(flat)
        means.fill(np.mean(flat))
        mean_diff = np.subtract(flat, means)

        time = np.linspace(0, flat.shape[0], flat.shape[0])
        p = np.poly1d(np.polyfit(time, flat, 5))
        from_fit = np.expand_dims(np.array(np.absolute(np.subtract(flat, p(time)))), axis=1)
        training.append(from_fit)
        train_ll.append(np.expand_dims(X_ll[count], axis=1))
    for count, i in enumerate(testX):
        flat = i.flatten()
        means = np.ones_like(flat)
        means.fill(np.mean(flat))
        mean_diff = np.subtract(flat, means)
        time = np.linspace(0, flat.shape[0], flat.shape[0])
        p = np.poly1d(np.polyfit(time, flat, 5))
        from_fit = np.expand_dims(np.array(np.absolute(np.subtract(flat, p(time)))), axis=1)
        testing.append(from_fit)
        test_ll.append(np.expand_dims(testX_ll[count], axis=1))
    return np.array(training), np.array(testing), np.array(train_ll), np.array(test_ll)

def getDataFFT(X, testX):
    training = []
    testing = []

    for i in X:
        flat = i.flatten()
        fft = np.absolute(np.fft.fft(flat))
        training.append(fft[1:-1])

    for i in testX:
        flat = i.flatten()
        fft = np.absolute(np.fft.fft(flat))
        testing.append(fft[1:-1])
    return np.array(training), np.array(testing)

def inString(s, files):
    for f in files:
        splitted = []
        split1 = f.split('_')
        for i in split1:
            splitted = splitted + i.split('.')
        if s in splitted:
            return f
    return False

def getCSVCount():
    dir = 'A1Benchmark'
    for s in sys.argv:
        if s[-4:] == "mark":
            dir = s
    path = 'data/ydata-labeled-time-series-anomalies-v1_0/' + dir + '/'
    files = [f for f in listdir(path) if isfile(join(path, f))]
    count = 0
    for f in files:
        if f[-3:] == 'csv':
            count+=1
    return count


def getInitModes(leave_out=None):
    FFT = False
    full_stats = False
    dir = 'A1Benchmark'
    name = 'mine'
    leave_one_out = None
    for s in sys.argv:
        if s[-4:] == "mark":
            dir = s
            break
    if 'full' in sys.argv:
        full_stats = True
        if 'fft' in sys.argv or 'FFT' in sys.argv:
            FFT = True
            if 'lstm' in sys.argv:
                print("FFT and LSTM")
                name = 'lstm'
                mod = getFFTLSTM(full_stats)
            elif 'unet' in sys.argv:
                print("FFT and UNET")
                name = 'unet'
                mod = getFFTUNET(full_stats)
            elif 'deepant' in sys.argv:
                print("FFT and DEEPANT")
                name = 'deepant'
                mod = getDeepANTFFT(full_stats, dir)
            elif 'mine' in sys.argv:
                name = 'mine'
                mod = getMyModelFFT(full_stats)
        elif 'unet' in sys.argv:
            name = 'unet'
            mod = getUnet(full_stats)
        elif 'deepant' in sys.argv:
            name = 'deepant'
            mod = getDeepANT(full_stats, dir)
        elif 'lstm' in sys.argv:
            name = 'lstm'
            mod = getLSTM_all_noFFT(full_stats)
        elif 'mine' in sys.argv:
            name = 'mine'
            mod = getMyModel(full_stats)
    else:
        if 'fft' in sys.argv or 'FFT' in sys.argv:
            FFT = True
            if 'lstm' in sys.argv:
                print("FFT and LSTM")
                name = 'lstm'
                mod = getFFTLSTM(full_stats)
            elif 'unet' in sys.argv:
                print("FFT and UNET")
                name = 'unet'
                mod = getFFTUNET(full_stats)
            elif 'deepant' in sys.argv:
                print("FFT and DEEPANT")
                name = 'deepant'
                mod = getDeepANTFFT(full_stats, dir)
            elif 'mine' in sys.argv:
                name = 'mine'
                mod = getMyModelFFT(full_stats)
        elif 'unet' in sys.argv:
            name = 'unet'
            mod = getUnet(full_stats)
        elif 'deepant' in sys.argv:
            name = 'deepant'
            mod = getDeepANT(full_stats, dir)
        elif 'lstm' in sys.argv:
            name = 'lstm'
            mod = getLSTM(full_stats)
        elif 'mine' in sys.argv:
            name = 'mine'
            mod = getMyModel(full_stats)

    if leave_out is None:
        path = 'data/ydata-labeled-time-series-anomalies-v1_0/' + dir + '/'
        files = [f for f in listdir(path) if isfile(join(path, f))]
        for s in sys.argv:
            leave_one_out = inString(s, files)
            if leave_one_out:
                break
    else:
        if dir[1] == '1':
            leave_one_out = 'real_' + str(leave_out) + '.csv'
        elif dir[1] == '2':
            leave_one_out = 'synthetic_' + str(leave_out) + '.csv'
        else:
            leave_one_out = dir + '-TS' + str(leave_out) + '.csv'

    return leave_one_out, dir, name, mod, FFT, full_stats
