import numpy as np
# import os
from os import listdir
from os.path import isfile, join
import exp_factory
from sklearn.utils import shuffle

augment = False

# arr1 has more than arr2
def balData(arr1, arr2, arr1_labels, arr2_labels, arr1_ll, arr2_ll, model):
    arr1, arr1_labels, arr1_ll = shuffle(arr1, arr1_labels, arr1_ll)
    arr1 = arr1[:arr2.shape[0]]
    arr1_labels = arr1_labels[:arr2.shape[0]]
    arr1_ll = arr1_ll[:arr2.shape[0]]
    if model == 'lstm' or model == 'deepant':
        return arr1, arr2, np.expand_dims(arr1_labels, axis=1), np.expand_dims(arr2_labels, axis=1), arr1_ll, arr2_ll
    return arr1, arr2, arr1_labels, arr2_labels, arr1_ll, arr2_ll

def balanceData(dataset, dataset_labels, dataset_label_layer, model):
    if dataset['anomalies'].shape[0] > dataset['non_anomalies'].shape[0]:
        anomalies, non_anomalies, anomalies_labels, nonanomalies_labels, anom_ll, nonanom_ll = balData(dataset['anomalies'], dataset['non_anomalies'], dataset_labels['anomalies'], dataset_labels['non_anomalies'], dataset_label_layer['anomalies'], dataset_label_layer['non_anomalies'], model)
    elif dataset['anomalies'].shape[0] < dataset['non_anomalies'].shape[0]:
        non_anomalies, anomalies, nonanomalies_labels, anomalies_labels, nonanom_ll, anom_ll = balData(dataset['non_anomalies'], dataset['anomalies'], dataset_labels['non_anomalies'], dataset_labels['anomalies'], dataset_label_layer['non_anomalies'], dataset_label_layer['anomalies'], model)

    print("Dataset balance: ", anomalies.shape, " ", non_anomalies.shape)
    return anomalies, non_anomalies, anomalies_labels, nonanomalies_labels, anom_ll, nonanom_ll

def shuffleAndOrganize(dataset, dataset_labels, dataset_label_layer, leave_one_out, model):
    lstm_flag = False
    anom_label = np.array([])
    nonanom_label = np.array([])
    if dataset_labels['non_anomalies'].size < 2:
        dataset_labels['non_anomalies'] = np.zeros_like(dataset['non_anomalies'])
    elif model == 'lstm' or model == 'deepant':
        print("should be lstm or deepant")
        lstm_flag = True
    elif model == 'unet':
        print("should be UNET")

    anomalies, non_anomalies, anomaly_labels, non_anomaly_labels, anom_ll, nonanom_ll = balanceData(dataset, dataset_labels, dataset_label_layer, model)

    anom_label = np.ones_like(anomaly_labels)
    nonanom_label = np.zeros_like(non_anomaly_labels)
    data = np.concatenate((anomalies, non_anomalies), axis=0)
    all_y = np.concatenate((anomaly_labels, non_anomaly_labels), axis=0)
    all_labels = np.concatenate((anom_label, nonanom_label), axis=0)
    all_ll = np.concatenate((anom_ll, nonanom_ll), axis=0)
    data, y, X_labels, data_ll = shuffle(data, all_y, all_labels, all_ll)

    if not leave_one_out:
        thresh = int(data.shape[0] * .75)
        X = data[:thresh]
        y = y[:thresh]
        X_ll = data_ll[:thresh]
        testX = data[thresh:]
        testy = data[thresh:]
        testX_ll = data[thresh:]
        return X, y, testX, testy, X_ll, testX_ll
    else:
        return data, y, np.array([]), np.array([]), X_labels, lstm_flag, data_ll

def lstmDatasetFactory(experiments, leave_one_out):
    full_trainX, full_trainy, full_trainlabels, full_train_ll = [], [], [], []
    for i, e in enumerate(experiments):
        full_trainX.append(e.non_anomalies)
        full_trainy.append(e.nonanom_next_val)
        full_trainlabels.append(e.nonanomalous_labels)
        full_train_ll.append(e.nonanom_label_layer)
        if e.id == leave_one_out:
            full_testX = e.samples
            full_testy = e.s_labels
            full_testlabels = e.testy
            full_test_ll = e.label_layer

    full_trainX = np.concatenate(full_trainX)
    full_trainy = np.concatenate(full_trainy)
    full_trainlabels = np.concatenate(full_trainlabels)
    full_train_ll = np.concatenate(full_train_ll)
    full_trainX, full_trainy, full_trainlabels, full_train_ll = shuffle(full_trainX, full_trainy, full_trainlabels, full_train_ll)
    return full_trainX, full_trainy, full_trainlabels, full_testX, full_testy, full_testlabels, full_train_ll, full_test_ll, experiments

def deepANTDatasetFactory(experiments, leave_one_out):
    full_trainX, full_trainy, full_trainlabels, full_train_ll = [], [], [], []
    for i, e in enumerate(experiments):
        full_trainX.append(e.trainX)
        full_trainy.append(e.trainy)
        full_trainlabels.append(e.train_labels)
        full_train_ll.append(e.train_label_layer)
        if e.id == leave_one_out:
            full_testX = e.testX
            full_testy = e.testy
            full_testlabels = e.test_labels
            full_test_ll = e.test_label_layer

    full_trainX = np.concatenate(full_trainX)
    full_trainy = np.concatenate(full_trainy)
    full_trainlabels = np.concatenate(full_trainlabels)
    full_train_ll = np.concatenate(full_train_ll)

    full_trainX, full_trainy, full_trainlabels, full_train_ll = shuffle(full_trainX, full_trainy, full_trainlabels, full_train_ll)
    return full_trainX, full_trainy, full_trainlabels, full_testX, full_testy, full_testlabels, full_train_ll, full_test_ll, experiments

def datasetFactory(experiments, leave_one_out, model):
    dataset = {'anomalies': np.array([]),
                'non_anomalies': np.array([])}

    dataset_label_layer = {'anomalies': np.array([]),
                            'non_anomalies': np.array([])}

    dataset_labels = {'anomalies': np.array([]),
                    'non_anomalies': np.array([])}

    for i, e in enumerate(experiments):
        if e.id == leave_one_out:
            leave_one_out_X = e.samples
            leave_one_out_y = e.s_labels
            leave_one_out_ll = e.label_layer
            try:
                testX_labels = e.testy
            except:
                pass
        if dataset['anomalies'].size == 0:
            dataset['anomalies'] = e.anomalies
            dataset_labels['anomalies'] = e.anomalous_labels
            dataset_label_layer['anomalies'] = e.anom_label_layer
        elif e.anomalies.size > 0:
            dataset['anomalies'] = np.concatenate((dataset['anomalies'], e.anomalies), axis=0)
            dataset_labels['anomalies'] = np.concatenate((dataset_labels['anomalies'], e.anomalous_labels), axis=0)
            dataset_label_layer['anomalies'] = np.concatenate((dataset_label_layer['anomalies'], e.anom_label_layer), axis=0)
        if dataset['non_anomalies'].size == 0:
            dataset['non_anomalies'] = e.non_anomalies
            dataset_labels['non_anomalies'] = e.nonanomalous_labels
            dataset_label_layer['non_anomalies'] = e.nonanom_label_layer
        elif e.anomalies.size > 0:
            dataset['non_anomalies'] = np.concatenate((dataset['non_anomalies'], e.non_anomalies), axis=0)
            dataset_labels['non_anomalies'] = np.concatenate((dataset_labels['non_anomalies'], e.nonanomalous_labels), axis=0)
            dataset_label_layer['non_anomalies'] = np.concatenate((dataset_label_layer['non_anomalies'], e.nonanom_label_layer), axis=0)

    X, y, testX, testy, X_labels, lstm_flag, X_ll = shuffleAndOrganize(dataset, dataset_labels, dataset_label_layer, leave_one_out, model)
    if not leave_one_out:
        return X, y, testX, testy

    testX = leave_one_out_X
    testX_ll = leave_one_out_ll
    if lstm_flag:
        testy = np.expand_dims(leave_one_out_y, axis=1)
        return X, y, testX, testy, X_labels, testX_labels, X_ll, testX_ll, experiments
    else:
        testy = leave_one_out_y
        return X, y, testX, testy, X_ll, testX_ll, experiments

def augmentExperiments(experiments, leave_one_out, model, dir, per_sample=3):
    new_experiments = []
    for i, e in enumerate(experiments):
        if e.id != leave_one_out:
            new_y, new_labels = e.augmentMe(per_sample)
            new_experiments.append(exp_factory.Experiment(e.id+str(i), e.x, new_y, new_labels, model=model, dir=dir))
    return new_experiments

def loadData(FFT, dir, leave_one_out, model):
    path = 'data/ydata-labeled-time-series-anomalies-v1_0/' + dir + '/'
    files = [f for f in listdir(path) if isfile(join(path, f))]

    time_idx = 0
    y_idx = 1
    anom_idx = 2
    experiments = []
    for f in files:
        if f[-3:] == 'csv':
            csv_name = f

            fname = path + f
            with open(fname) as f:
                lines = (line for line in f if not line.startswith('#'))
                arr = np.loadtxt(lines, delimiter=',', skiprows=1)
            time = np.array(list(range(len(arr[:,time_idx]))))
            if leave_one_out == csv_name:
                is_test = True
            else:
                is_test = False
            experiments.append(exp_factory.Experiment(csv_name, time, arr[:,y_idx], arr[:,anom_idx], model=model, dir=dir, is_test=is_test))

    if augment:
        new_experiments = augmentExperiments(experiments, leave_one_out, dir, model)
        experiments = experiments + new_experiments

    if model == 'deepant':
        return deepANTDatasetFactory(experiments, leave_one_out)
    elif model == 'lstm':
        return lstmDatasetFactory(experiments, leave_one_out)
    else:
        return datasetFactory(experiments, leave_one_out, model)
