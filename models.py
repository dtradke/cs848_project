import numpy as np
import os
import numpy as np
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as keras


def unet_fft(full_stats, pretrained_weights = None,input_size = (32,1)):
    if full_stats:
        n_timesteps, n_features = 32, 3#4
    else:
        n_timesteps, n_features = 32, 1

    fft_inputs = Input(((n_timesteps-2), ), name='fft_Input')
    inputs = Input((n_timesteps, n_features), name='Input')
    conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = MaxPooling1D(pool_size=2)(conv2)
    conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = MaxPooling1D(pool_size=2)(conv3)
    conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling1D(pool_size=2)(drop4)


    fft_dense = Dense(64, activation = 'relu')(fft_inputs)
    fft_dense = Dense(128, activation = 'relu')(fft_dense)
    fft_dense = Dense(256, activation = 'relu')(fft_dense)
    fft_dense = Dense(512, activation = 'relu')(fft_dense)
    fft_dense = Dense(1024, activation = 'relu')(fft_dense)

    conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = Flatten()(conv5)
    concated = concatenate([conv5, fft_dense], axis = 1)

    concated = Dense(2048, activation = 'relu')(concated)
    concated = Dense(5096, activation = 'relu')(concated)
    concated = Dense(2048, activation = 'relu')(concated)
    concated = Reshape((2,1024))(concated)

    conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(concated)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv1D(filters=512, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(drop5))
    merge6 = concatenate([drop4,up6], axis = 2)
    conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = Conv1D(filters=256, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv6))
    merge7 = concatenate([conv3,up7], axis = 2)
    conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = Conv1D(filters=128, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv7))
    merge8 = concatenate([conv2,up8], axis = 2)
    conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = Conv1D(filters=64, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv8))
    merge9 = concatenate([conv1,up9], axis = 2)
    conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = Conv1D(filters=2, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = Conv1D(filters=1, kernel_size=1, activation = 'sigmoid')(conv9)
    model = Model(input = [inputs, fft_inputs], output = conv10)

    model.compile(optimizer = Adam(lr = 1e-4), loss = 'binary_crossentropy', metrics = ['accuracy'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)

    return model


def unet(full_stats, pretrained_weights = None,input_size = (32,1)):
    if full_stats:
        n_timesteps, n_features = 32, 3#4
    else:
        n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = MaxPooling1D(pool_size=2)(conv2)
    conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = MaxPooling1D(pool_size=2)(conv3)
    conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling1D(pool_size=2)(drop4)

    conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv1D(filters=512, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(drop5))
    merge6 = concatenate([drop4,up6], axis = 2)
    conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = Conv1D(filters=256, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv6))
    merge7 = concatenate([conv3,up7], axis = 2)
    conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = Conv1D(filters=128, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv7))
    merge8 = concatenate([conv2,up8], axis = 2)
    conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = Conv1D(filters=64, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv8))
    merge9 = concatenate([conv1,up9], axis = 2)
    conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = Conv1D(filters=2, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = Conv1D(filters=1, kernel_size=1, activation = 'sigmoid')(conv9)
    model = Model(input = inputs, output = conv10)

    model.compile(optimizer = Adam(lr = 1e-4), loss = 'binary_crossentropy', metrics = ['accuracy'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)

    return model


def deepant_fft(full_stats, dir, pretrained_weights=None,input_size=(45,1)):
    if dir == 'A1Benchmark' or dir == 'A2Benchmark':
        size = 45
    else:
        size = 35
    if full_stats:
        n_timesteps, n_features = size, 3#4
    else:
        n_timesteps, n_features = size, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    fft_inputs = Input(((n_timesteps-2), ), name='fft_Input')
    conv1 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(inputs)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv2 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(pool1)
    pool2 = MaxPooling1D(pool_size=2)(conv2)
    flat = Flatten()(pool2)

    fft_dense = Dense(32, activation = 'relu')(fft_inputs)
    fft_dense = Dense(64, activation = 'relu')(fft_dense)
    concated = concatenate([flat, fft_dense], axis = 1)
    concated = Dense(64, activation = 'relu')(concated)

    dense = Dense(32, activation = 'relu')(concated)
    out = Dense(1, activation='relu')(dense)

    model = Model(input = [inputs, fft_inputs], output = out)

    model.compile(optimizer = Adam(lr = 1e-4), loss = 'mean_squared_error', metrics = ['mse'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model


def deepant(full_stats, dir, pretrained_weights=None,input_size=(45,1)):
    if dir == 'A1Benchmark' or dir == 'A2Benchmark':
        size = 45
    else:
        size = 35

    if full_stats:
        n_timesteps, n_features = size, 3#4
    else:
        n_timesteps, n_features = size, 1


    inputs = Input((n_timesteps, n_features), name='Input')
    conv1 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(inputs)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv1 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(pool1)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    flat = Flatten()(pool1)
    dense = Dense(32, activation = 'relu')(flat)
    out = Dense(1, activation='relu')(dense)

    model = Model(input = inputs, output = out)

    model.compile(optimizer = Adam(lr = 1e-4), loss = 'mean_squared_error', metrics = ['mse'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model

def lstm_fft(pretrained_weights=None,input_size=(8,1)):
    n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    fft_inputs = Input(((n_timesteps-2), ), name='fft_Input')

    lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(inputs)
    lstm2 = LSTM(64, activation = 'relu')(lstm1)

    fft_dense = Dense(32, activation = 'relu')(fft_inputs)
    fft_dense = Dense(64, activation = 'relu')(fft_dense)
    concat = Concatenate(name='ref_fft_concat', axis=1)([lstm2, fft_dense])
    out = Dense(1, activation='relu')(concat)

    model = Model(input = [inputs,fft_inputs], output = out)

    sgd = SGD(lr=0.01, clipvalue=0.5)
    model.compile(loss='mean_squared_error', optimizer='adam')

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model

def lstm_all_noFFT(full_stats, pretrained_weights=None,input_size=(32,1)):
    n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    full_inputs = Input((n_timesteps, n_features), name='full_Input')
    past_label = Input((n_timesteps, n_features), name='label_Input')

    lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(inputs)
    lstm2 = LSTM(64, activation = 'relu')(lstm1)

    full_lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(full_inputs)
    full_lstm2 = LSTM(64, activation = 'relu')(full_lstm1)

    past_lstm = LSTM(32, activation = 'relu', return_sequences=True)(past_label)
    past_lstm = LSTM(64, activation = 'relu')(past_lstm)

    concat_lstm = Concatenate(name='ref_diff_concat', axis=1)([lstm2, full_lstm2])
    concat = Concatenate(name='ref_concat2', axis=1)([concat_lstm, past_lstm])

    out = Dense(64, activation='relu')(concat)
    out = Dense(1, activation='relu')(concat)

    model = Model(input = [inputs, full_inputs,past_label], output = out)

    sgd = SGD(lr=0.01, clipvalue=0.5)
    model.compile(loss='mean_squared_error', optimizer='adam')

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model


def lstm_all(pretrained_weights=None,input_size=(8,1)):
    n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    full_inputs = Input((n_timesteps, n_features), name='full_Input')
    fft_inputs = Input(((n_timesteps-2), ), name='fft_Input')
    past_label = Input((n_timesteps, n_features), name='label_Input')

    lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(inputs)
    lstm2 = LSTM(64, activation = 'relu')(lstm1)

    full_lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(full_inputs)
    full_lstm2 = LSTM(64, activation = 'relu')(full_lstm1)

    fft_dense = Dense(32, activation = 'relu')(fft_inputs)
    fft_dense = Dense(64, activation = 'relu')(fft_dense)

    past_lstm = LSTM(32, activation = 'relu', return_sequences=True)(past_label)
    past_lstm = LSTM(64, activation = 'relu')(past_lstm)

    concat_lstm = Concatenate(name='ref_diff_concat', axis=1)([lstm2, full_lstm2])
    concat = Concatenate(name='ref_concat2', axis=1)([concat_lstm, past_lstm])
    concat = Concatenate(name='ref_fft_concat', axis=1)([concat, fft_dense])

    out = Dense(64, activation='relu')(concat)
    out = Dense(1, activation='relu')(concat)

    # model = Model(input = [inputs, full_inputs,past_label,fft_inputs], output = out)
    model = Model(input = [inputs, full_inputs,past_label,fft_inputs], output = out)

    sgd = SGD(lr=0.01, clipvalue=0.5)
    model.compile(loss='mean_squared_error', optimizer='adam')

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model


# makes prediction for next step, then needs to check how far it is
def lstm(pretrained_weights=None,input_size=(8,1)):
    n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    lstm1 = LSTM(32, activation = 'relu', return_sequences=True)(inputs)
    lstm2 = LSTM(64, activation = 'relu')(lstm1)
    out = Dense(1, activation='relu')(lstm2)

    model = Model(input = inputs, output = out)

    sgd = SGD(lr=0.01, clipvalue=0.5)
    model.compile(optimizer = 'adam', loss = 'mean_squared_error')

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model

def my_model_fft(full_stats, pretrained_weights=None,input_size=(45,1)):
    if full_stats:
        n_timesteps, n_features = 32, 3#4
    else:
        n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    inputs_fft = Input((n_timesteps-2, ), name='fft_Input')

    conv1 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(inputs)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv2 = Conv1D(filters=64, kernel_size=3, activation = 'relu')(pool1)
    flat = Flatten()(conv2)

    dense_fft = Dense(64, activation = 'relu')(inputs_fft)
    dense_fft = Dense(32, activation = 'relu')(dense_fft)
    concat = Concatenate(name='ref_fft_concat', axis=1)([flat, dense_fft])

    dense = Dense(64, activation = 'relu')(concat)
    dense = Dense(32, activation = 'relu')(dense)
    out = Dense(2, activation = 'softmax')(dense)

    model = Model(input = [inputs, inputs_fft], output = out)

    opt = SGD(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model

def my_model(full_stats, pretrained_weights=None,input_size=(32,1)):
    if full_stats:
        n_timesteps, n_features = 32, 3#4
    else:
        n_timesteps, n_features = 32, 1

    inputs = Input((n_timesteps, n_features), name='Input')
    conv1 = Conv1D(filters=32, kernel_size=3, activation = 'relu')(inputs)
    pool1 = MaxPooling1D(pool_size=2)(conv1)
    conv2 = Conv1D(filters=64, kernel_size=3, activation = 'relu')(pool1)
    flat = Flatten()(conv2)
    dense = Dense(64, activation = 'relu')(flat)
    dense = Dense(32, activation = 'relu')(dense)
    out = Dense(2, activation = 'softmax')(dense)

    model = Model(input = inputs, output = out)

    opt = SGD(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)
    return model
