# # import numpy as np
# import matplotlib.pyplot as plt
# from matplotlib.ticker import MaxNLocator
# from collections import namedtuple
# import numpy as np
#
# N = 10
# # firecast = (87.7, 89.3, 6.9)
# # farsite_wet = (82.3, 93.8, 6)
# # farsite_dry = (73.4, 78.4, 3.1)
# # random = (49.9, 2.3, 0.44)
#
# test_points = (0.04, 6.8, 20.9, 24.3, 43.4, 53.3, 49.5, 40.1, 57.3, 72)
# test_site = (0.4, 4.2, 42.4, 33.3, 44, 32.5, 21.3, 9.6, 1.8, 0.1)
#
# # regress = (36.8, 22.2)
# # bin_class = (81.2, 79)
# # classify = (59.4, 28.4)
#
#
# ind = np.arange(N)
# width = 0.4
# plt.bar(ind, test_points, width, label='Test Set')
# plt.bar(ind + width, test_site, width, label='Test Site')
# # plt.bar(ind + (2*width), classify, width, label='Four-Class Classification')
# # plt.bar(ind + (3*width), random, width, label='Random Pred.')
#
# plt.ylabel('Percent Accurate', fontsize = 20)
# plt.xlabel('Dataset Classes by Height (feet)', fontsize = 20)
# plt.title('Regression Breakdown', fontsize=23)
#
# plt.ylim(top=100)  # adjust the top leaving bottom unchanged
# plt.ylim(bottom=0)
#
# plt.yticks(fontsize=15)
# plt.xticks(ind + width / 2, ('<2', '2-5', '5-10', '10-20', '20-30', '30-40', '40-50', '50-75', '75-100', '100 <='), fontsize=15) #, 'F-Score'
# plt.legend(loc='best', fontsize=12)
# plt.show()


import matplotlib.pyplot as plt
plt.style.use('ggplot')
import numpy as np

plt.figure(figsize=(9, 3))

N = 4
# norm = (0.2, 0.9, 0.85, 0.82) #UNET
# fft = (0.3, 0.83, 0.82, 0.84)
# all_nofft = (0.04, 0.14, 0.16, 0.15)
# all = (0.88, 0.67, 0.83, 0.83)
# norm = (0.53, 0.98, 0.99, 0.95) #CNN
# fft = (0.4, 0.83, 0.94, 0.92)
# all_nofft = (0.16, 0.59, 0.75, 0.53)
# all = (0.17, 0.67, 0.74, 0.97)

# norm = (0.34, 0.04, 0.07, 0.02) #Deepant custthresh
# fft = (0.29, 0.06, 0.05, 0.04)
# all_nofft = (0.28, 0.04, 0.92, 0.14)
# all = (0.24, 0.07, 0.14, 0.04)
#
# fft = np.subtract(fft, norm)
# all_nofft = np.subtract(all_nofft, norm)
# all = np.subtract(all, norm)

# OVERFITTING
total = (67, 100, 100, 100)
# norm = (5, 0, 0, 0) #UNET
# fft = (6, 0, 0, 0)
# all_nofft = (0, 0, 0, 0)
# all = (0, 0, 0, 0)

# norm = (5,0,0,0) #CNN
# fft = (0,0,0,0)
# all_nofft = (2,3,1,0)
# all = (2,0,0,2)

norm = (0,23,13,22) #deepant
fft = (0,13,23,14)
all_nofft = (3,23,0,2)
all = (3,12,7,15)

norm = np.subtract(total, norm)
fft = np.subtract(np.subtract(total, fft), norm)
all_nofft = np.subtract(np.subtract(total, all_nofft), norm)
all = np.subtract(np.subtract(total, all), norm)



ind = np.arange(N)
width = 0.25
# plt.bar(ind, norm, width, label='Normal')
plt.bar(ind, fft, width, label='FFT-Mode')
plt.bar(ind + (width), all_nofft, width, label='ALL-NoFFT')
plt.bar(ind + (width*2), all, width, label='ALL-Mode')

plt.axhline(y=0, c='black', linewidth=1, label="Normal")
plt.ylabel('F1-Score', fontsize=22, c='black')
plt.ylim(top=25)
#plt.title('Scores by group and gender')
plt.yticks(fontsize=22, c='black')
plt.xticks(ind + width , ('A1', 'A2', 'A3', 'A4'), fontsize=22, c='black')
plt.legend(loc=2, fontsize=14, ncol=2)
plt.show()
