import sys
import numpy as np
import tensorflow as tf
import process_data
from os import listdir
from os.path import isfile, join
import models
import math
import time
import util
from sklearn.cluster import KMeans

loaded_model = False

all_TP = 0
all_TN = 0
all_FP = 0
all_FN = 0

def getModelandData(leave_one_out, dir, name, mod, FFT):
    print("leave_one_out: ", leave_one_out)

    if name == 'lstm' or name == 'deepant':
        X, y, trainX_labels, testX, testy, testX_labels, X_ll, testX_ll, experiments = process_data.loadData(FFT, dir, leave_one_out, name)
    else:
        X, y, testX, testy, X_ll, testX_ll, experiments = process_data.loadData(FFT, dir, leave_one_out, name)

    if name == 'lstm' or name == 'deepant':
        return mod, X, y, trainX_labels, testX, testy, testX_labels, name, leave_one_out, FFT, X_ll, testX_ll, experiments
    return mod, X, y, [], testX, testy, {}, name, leave_one_out, FFT, X_ll, testX_ll, experiments

def trainAndEvaluateDeepAnT(mod, X, y, train_labels, testX, testy, test_labels, name, leave_one_out, FFT, full_stats, X_ll, testX_ll, dir, leave_one_out_exp):

    if FFT:
        print("Getting FFT")
        fft_X, fft_testX = util.getDataFFT(X, testX)
    if full_stats:
        print("Should be full_stats")
        # lag_X, lag_testX, X, testX = util.getLag(X, testX)
        X, testX = util.getDataStats(X, testX, X_ll, testX_ll)

    if FFT:
        X = [X, fft_X]
        testX = [testX, fft_testX]

    # print("Final Shape: ", X[0].shape, " test: ", testX[0].shape)

    if not loaded_model:
        mod.fit(X, y, batch_size=32, epochs=5, verbose=1) #, validation_split=0.1 CHANGED epochs to 5

    y_preds = mod.predict(testX)

    anom_diff = []
    nonanom_diff = []
    full_diff = []
    # thresh = 0.4

    # if dir == 'A1Benchmark':
    #     thresh = 0.5
    # elif dir == 'A2Benchmark':
    #     thresh = 0.75
    # elif dir == 'A3Benchmark':
    #     thresh = 0.65
    # elif dir == 'A4Benchmark':
    #     thresh = 0.55
    thresh = leave_one_out_exp.threshold

    TP = 0 #anomaly
    FP = 0 #non-anomaly classified as an anomaly
    TN = 0 #non-anomaly
    FN = 0 #anomaly missed by model
    gap_arr = []

    y_preds = y_preds.flatten()

    for i, val in enumerate(testy):
        diff = abs(val - y_preds[i])
        if diff > thresh and test_labels[i] > 0: #identified anomaly
            TP+=1
        elif diff < thresh and test_labels[i] > 0: #missed anomaly
            FN+=1
        elif diff > thresh and test_labels[i] == 0:
            FP+=1
        elif diff < thresh and test_labels[i] == 0:
            TN+=1

    return util.Result(leave_one_out, mod, TP, TN, FP, FN)

def trainAndEvaluateLSTM(mod, X, y, train_labels, testX, testy, test_labels, name, leave_one_out, FFT, full_stats, X_ll, testX_ll, leave_one_out_exp):

    if FFT:
        fft_X, fft_testX = util.getDataFFT(X, testX)
    if full_stats:
        X_diff, testX_diff, X_ll, testX_ll = util.getDataStatsLSTM(X, testX, X_ll, testX_ll)
        X = [X, X_diff, X_ll]
        testX = [testX, testX_diff, testX_ll]

    if FFT and type(X) == list:
        X = X + [fft_X]
        testX = testX + [fft_testX]
    elif FFT and type(X) != list:
        X = [X, fft_X]
        testX = [testX, fft_testX]


    if not loaded_model:
        mod.fit(X, y, batch_size=32, epochs=10, verbose=1)
        # time_string = time.strftime("%Y%m%d-%H%M%S")
        # fname = 'models/' + name + time_string + '_without-' + leave_one_out[:-4] + '.h5'
        # print("Saving: ", fname)
        # mod.save_weights(fname)
    y_preds = mod.predict(testX)

    anom_diff = []
    nonanom_diff = []
    full_diff = []
    thresh = leave_one_out_exp.threshold #0.4 #NOTE: was .15
    # thresh = 0.4
    TP = 0 #anomaly
    FP = 0 #non-anomaly classified as an anomaly
    TN = 0 #non-anomaly
    FN = 0 #anomaly missed by model

    for i, val in enumerate(testy):
        diff = abs(val - y_preds[i][0])
        if diff > thresh and test_labels[i] > 0: #identified anomaly
            TP+=1
        elif diff < thresh and test_labels[i] > 0: #missed anomaly
            FN+=1
        elif diff > thresh and test_labels[i] == 0:
            FP+=1
        elif diff < thresh and test_labels[i] == 0:
            TN+=1

    return util.Result(leave_one_out, mod, TP, TN, FP, FN)


def trainAndEvaluateMINE(mod, X, y, testX, testy, name, leave_one_out, FFT, full_stats, X_ll, testX_ll):
    global all_FN
    global all_FP
    global all_TN
    global all_TP

    y = util.flat_y(y)
    testy = util.flat_y(testy)

    if FFT:
        fft_X, fft_testX = util.getDataFFT(X, testX)
    if full_stats:
        X, testX = util.getDataStats(X, testX, X_ll, testX_ll)
    if FFT:
        X = [X, fft_X]
        testX = [testX, fft_testX]

    if not loaded_model:
        mod.fit(X, y, batch_size=32, epochs=100, verbose=1)
        # time_string = time.strftime("%Y%m%d-%H%M%S")
        # fname = 'models/' + name + time_string + '_without-' + leave_one_out[:-4] + '.h5'
        # print("Saving: ", fname)
        # mod.save_weights(fname)

    y_preds = mod.predict(testX)
    y_classified = []

    TP = 0 #anomaly
    FP = 0 #non-anomaly classified as an anomaly
    TN = 0 #non-anomaly
    FN = 0 #anomaly missed by model
    last = 0
    for i, ground in enumerate(testy):
        actual = np.argmax(ground)
        pred = np.argmax(y_preds[i])
        if actual == pred and actual == 0:
            TN+=1
        elif actual == pred and actual == 1:
            TP+=1
        elif actual != pred and actual == 1:
            FN+=1
        elif actual != pred and actual == 0:
            FP+=1

    return util.Result(leave_one_out, mod, TP, TN, FP, FN)

def trainAndEvaluateUNET(mod, X, y, testX, testy, name, leave_one_out, FFT, full_stats, X_ll, testX_ll):
    global all_FN
    global all_FP
    global all_TN
    global all_TP

    if FFT:
        fft_X, fft_testX = util.getDataFFT(X, testX)
    if full_stats:
        X, testX = util.getDataStats(X, testX, X_ll, testX_ll)
    if FFT:
        X = [X, fft_X]
        testX = [testX, fft_testX]

    if not loaded_model:
        mod.fit(X, y, batch_size=32, epochs=15, verbose=1)
        # time_string = time.strftime("%Y%m%d-%H%M%S")
        # fname = 'models/' + name + time_string + '_without-' + leave_one_out[:-4] + '.h5'
        # print("Saving: ", fname)
        # mod.save_weights(fname)

    y_preds = mod.predict(testX)
    y_classified = []

    thresh = 0.5

    TP = 0 #anomaly
    FP = 0 #non-anomaly classified as an anomaly
    TN = 0 #non-anomaly
    FN = 0 #anomaly missed by model
    for i, ground in enumerate(testy):
        for j, val in enumerate(ground):
            if val > thresh and y_preds[i][j] > thresh:
                TP+=1
            elif val > thresh and y_preds[i][j] < thresh:
                FN+=1
            elif val < thresh and y_preds[i][j] > thresh:
                FP+=1
            elif val < thresh and y_preds[i][j] < thresh:
                TN+=1

    return util.Result(leave_one_out, mod, TP, TN, FP, FN)

if __name__ == "__main__":
    results = {}
    len_files = util.getCSVCount()

    for leave_out in range(1,len_files+1):
        leave_one_out, dir, name, mod, FFT, full_stats = util.getInitModes(leave_out)
        results[leave_one_out[:-4]] = []
        print("Testing: ", leave_one_out[:-4])
        for loop in range(0,1):
            print("test count: ", loop+1)
            mod, X, y, train_labels, testX, testy, test_labels, name, leave_one_out, fft, X_ll, testX_ll, experiments = getModelandData(leave_one_out, dir, name, mod, FFT) #leave_out
            for e in experiments:
                if e.is_test:
                    leave_one_out_exp = e
                    break
            if name == 'deepant':
                results[leave_one_out[:-4]].append(trainAndEvaluateDeepAnT(mod, X, y, train_labels, testX, testy, test_labels, name, leave_one_out, FFT, full_stats, X_ll, testX_ll, dir, leave_one_out_exp))
            elif name == 'lstm':
                results[leave_one_out[:-4]].append(trainAndEvaluateLSTM(mod, X, y, train_labels, testX, testy, test_labels, name, leave_one_out, FFT, full_stats, X_ll, testX_ll, leave_one_out_exp))
            elif name == 'unet':
                results[leave_one_out[:-4]].append(trainAndEvaluateUNET(mod, X, y, testX, testy, name, leave_one_out, FFT, full_stats, X_ll, testX_ll))
            elif name == 'mine':
                results[leave_one_out[:-4]].append(trainAndEvaluateMINE(mod, X, y, testX, testy, name, leave_one_out, FFT, full_stats, X_ll, testX_ll))

    best_results = util.findBestResults(results)
    for i in best_results:
        if (i.tp + i.fn) != 0:
            all_TP+=i.tp
            all_TN+=i.tn
            all_FP+=i.fp
            all_FN+=i.fn

    print("======================================================================================")
    print("ALL: ")
    print("TP: ", all_TP, " FN: ", all_FN, " FP: ", all_FP, " TN: ", all_TN)
    print("=======================================")
    try:
        print("Percent of actual anomalied detected:        ", all_TP / (all_TP + all_FN))
        print("Correct anomalies classified being correct:  ", all_TP / (all_TP + all_FP))
        print("Real anomalies:                              ", all_TP + all_FN)
    except:
        print("Division by 0")
        print("Real anomalies:                              ", all_TP + all_FN)
    len_best_results = len(best_results)
    best_results.append(util.ResultSummary(dir, name, all_TP, all_TN, all_FP, all_FN))
    full_precision = 0
    full_recall = 0
    full_f1 = 0
    for i in best_results:
        if i.precision == 1:
            full_precision+=1
        if i.recall == 1:
            full_recall+=1
        if i.f1 == 1:
            full_f1+=1
        print(i)
    print("Full amounts -- precision: ", full_precision, " recall: ", full_recall, " f1: ", full_f1)
    util.printAndWrite(best_results, name)

# TO RUN
# python3 main.py [model] [fft or fft full or None] [dir_name or None]
